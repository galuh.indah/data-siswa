<?php
/* Fungsi Konversi nilai ke huruf */

if (isset($_POST['submit'])) {
    $nama  = $_POST["nama"]; //mengambil inputan yang telah dimasukkan dan mengidentifikasinya menjadi $nama
    $mapel  = $_POST["mapel"]; //mengambil inputan yang telah dimasukkan dan mengidentifikasinya menjadi $mapel
    $uts  = $_POST["nilaiuts"]; //mengambil inputan yang telah dimasukkan dan mengidentifikasinya menjadi $uts
    $uas  = $_POST["nilaiuas"]; //mengambil inputan yang telah dimasukkan dan mengidentifikasinya menjadi $uas
    $tugas  = $_POST["nilaitugas"]; //mengambil inputan yang telah dimasukkan dan mengidentifikasinya menjadi $tugas

     //mencari nilai akhir
    $nilai  = ($tugas * 0.15) + ($uts * 0.35) + ($uas * 0.50); //menghitung nilai akhir
    $grade  = grade($nilai);
    //menampilkan grade dari nilai
    function grade($nilai)
    {
        if ($nilai <= 100) {
            $grade = "A";
        }
        if ($nilai <  80) {
            $grade = "B";
        }
        if ($nilai <  70) {
            $grade = "C";
        }
        if ($nilai <  60) {
            $grade = "D";
        }
        if ($nilai <  50) {
            $grade = "E";
        }

        return $grade;
    }
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Data Nilai</title>
</head>
<style>
    .container {
        background: #DCDCDC //memberikan warna pada tabel
    }
</style>

<body>
//membuat tombol submit yang akan memproses dari php -->
    <?php if (isset($_POST['submit'])) : ?>
        <div class="container border border-primary">
            <br>
            <center>
                <h2>Hasil Nilai Mahasiswa</h2>
            </center>
            <table class="table">
                <tr>
                    <td class="col-sm-2">Nama</td>
                    <td class="col-sm-4">: <?php echo $nama; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Mata Pelajaran</td>
                    <td class="col-sm-4">: <?php echo $mapel; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Nilai UTS</td>
                    <td class="col-sm-4">: <?php echo $uts; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Nilai UAS</td>
                    <td class="col-sm-4">: <?php echo $uas; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Nilai Tugas</td>
                    <td class="col-sm-4">: <?php echo $tugas; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Total Nilai</td>
                    <td class="col-sm-4">: <?php echo $nilai; ?></td>
                </tr>
                <tr>
                    <td class="col-sm-2">Grade</td>
                    <td class="col-sm-4">: <?php echo $grade; ?></td>
                </tr>
            </table>
            <div class="mb-3">
                <button class="btn btn-primary" type="button">
                    <a class="text-decoration-none text-light" href="inputnilai.php">Kembali</a>
                </button>
            </div>

            <br>
        </div>
    <?php endif; ?>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
</body>

</html>