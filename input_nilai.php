<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" 
    rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <style>
  .container{
    background: #DCDCDC
  }
  </style>
  <body>
  
   <center>
<h2>Data Nilai Siswa/Siswi</h2>
<p >Selamat Datang di Program Input Data</p>
<p >Silakan Input Data</p>
</center>


<div class="container "m>
<div class="row align-items-start">
<div class="col-25 border border-primary mt-1 p-3"> //membuat form yang berukuran 25 dalam row dengan margin bottom 3 
<form action="hasil.php" method="post">
<form class="mb-3 row">
<div class="row">
    <label for="nama" class="col-sm-2 col-form-label">NAMA</label>
    <div class="form-mb-20">
      <input type="text" class="form-control" id="nama" name="nama" placeholder="input nama">
    </div><br>
    <label for="mapel" class="col-sm-2 col-form-label">MATA PELAJARAN</label><br>
    <div class="form-mb-1">
    <select class="form-select" id="mapel" name="mapel" aria-label="Default select example">
         <option selected></option>
         <option value="Matematika">Matematika</option>
         <option value="IPA">IPA</option>
         <option value="IPS">IPS</option>
         <option value="Bahasa">Bahasa</option>
         
         //membuat form dalam bentuk type inputan select -->    
    </select>
    </div>
    <label for="nilaiuts" class="col-sm-2 col-form-label">NILAI UTS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaiuts"name="nilaiuts" value="nilaiuts" placeholder="input nilai" >
    </div><br>
    <label for="nilaiuas" class="col-sm-2 col-form-label">NILAI UAS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaiuas" name="nilaiuas" value="nilaiuas" placeholder="input nilai" >
    </div><br>
    <label for="nilaitugas" class="col-sm-2 col-form-label">NILAI TUGAS</label>
    <div class="form-mb-1">
      <input type="number" class="form-control" id="nilaitugas" name="nilaitugas" value="nilaitugas" placeholder="input nilai" >
    </div>
  <div class="d-grid gap-2"><br>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div><br>
  <div class="d-grid gap-12">
  <button type="reset">Reset</button>
  </div>
  </form>
            //membuat tombol submit yang akan memproses dari php -->
  <?php if(isset($_GET['submit'])): ?>
        <div class="row justify-content-center"> //membuat agar konten tidak mengisi seluruh layar dan berposisi ditengah
            <div class="col-8 border border-primary mt-3 p-3"> //membuat form yang berukuran 8 dalam row dengan margin bottom 3 
              //dan juga membuat form dengan metode dan action dikosongkan karna prosesnya masih dalam satu file yang sama -->
                <div class="alert alert-success">
                Hasil nilai uts Adalah <?php echo $nilaiuts ?> <br>
                Hasil nilai uas Adalah <?php echo $nilaiuas ?> <br>
                Hasil nilai tugas Adalah <?php echo $nilaitugas ?> <br>
                </div>
            </div>
        </div>
  <?php endif; ?>
  
</div>  //optional javascrip; choose one of the two! -->
</div>
</div>
 

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>